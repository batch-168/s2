<?php require_once "./code.php"; ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>S02: Repetition Control Structures and Array Manipulation</title>
</head>
<body>
	<h1>Repetition Control Structures</h1>
	<h2>Divisible Five</h2>

	<?php modifiedForLoop(); ?>
	
	<h2>Arrays</h2>

	<pre>Names <?php print_r($names); ?></pre>
	<pre>Count: <?php echo count($names); ?></pre>


	<?php array_push($names, 'John Smith') ?>

	<pre>Names <?php print_r($names); ?></pre>
	<pre>Count: <?php echo count($names); ?></pre>

	<?php array_push($names, 'Jane Smith') ?>

	<pre>Names <?php print_r($names); ?></pre>
	<pre>Count: <?php echo count($names); ?></pre>

	<?php array_shift($names); ?>
	<pre>Names <?php print_r($names); ?></pre>
	<pre>Count: <?php echo count($names); ?></pre
</body>
</html>